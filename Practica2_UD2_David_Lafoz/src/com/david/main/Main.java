package com.david.main;

import com.david.gui.Controlador;
import com.david.gui.Modelo;
import com.david.gui.Vista;

import java.sql.SQLException;

/**
 * Sirve para llamar a las clases principales y a la interfaz
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo,vista);
    }

}
