package com.david.base_enums;

/**
 * Es el enum de todas las ciudades
 */

public enum ClienteCiudad {
    BARCELONA("Barcelona"),
    ZARAGOZA("Zaragoza"),
    MADRID("Madrid"),
    SEVILLA("Sevilla"),
    LEON("Leon"),
    HUESCA("Huesca"),
    TERUEL("Teruel");


    String ciudad;
    ClienteCiudad(String ciudad) {
        this.ciudad=ciudad;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
