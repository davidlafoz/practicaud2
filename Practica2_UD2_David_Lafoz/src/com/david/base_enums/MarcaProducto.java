package com.david.base_enums;

/**
 * Es el enum de todos los productos
 */

public enum MarcaProducto {

    NIVEA("Nivea"),
    AVENE("Avene"),
    LAROCHE("Laroche"),
    LOREAL("Loreal"),
    CHANEL("Chanel"),
    MECCA("Mecca");

    String marca;
    MarcaProducto(String marca) {
        this.marca=marca;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
