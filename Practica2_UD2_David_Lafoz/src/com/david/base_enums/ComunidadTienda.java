package com.david.base_enums;

public enum ComunidadTienda {

    /**
     * Es el enum de todas las comunidades
     */
    ARRAGON("Aragon"),
    CATALUNA("Cataluña"),
    MADRID("Madrid"),
    CASTILLAYLEON("Castilla y Leon"),
    ANDALUCIA("Andalucia");


    String comunidad;
    ComunidadTienda(String comunidad) {
        this.comunidad=comunidad;

    }

    public String getComunidad() {
        return comunidad;
    }

    public void setComunidad(String comunidad) {
        this.comunidad = comunidad;
    }
}
