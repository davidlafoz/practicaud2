package com.david.base_enums;

/**
 * Clase enumerada en la que muestra todas las provincias
 */
public enum TiendaProvincia {
    BARCELONA("Barcelona"),
    ZARAGOZA("Zaragoza"),
    MADRID("Madrid"),
    SEVILLA("Sevilla"),
    LEON("Leon"),
    HUESCA("Huesca"),
    TERUEL("Teruel");

    String provincia;

    TiendaProvincia(String provincia) {
        this.provincia=provincia;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}
