package com.david.base_enums;

/**
 * Enum de dos tablas para buscar
 */

public enum Buscar {

    PRODUCTOS("productos"),
    CLIENTES("cliente");

    String valor;

    Buscar(String valor) {
       this.valor=valor;
    }

    public String getValor() {
        return valor;
    }
}
