package com.david.gui;

import com.david.base_enums.Buscar;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

/**
 * Interfaz para buscar un producto o cliente
 */
public class Consultar extends JFrame{
    public JPanel panel1;
    public JTextField JTNombreBuscar;
    public JTextField JTBuscarPorNombre;
    public JComboBox CBComboBuscar;
    public JButton BuscarBtn;
    private JLabel EdadPrecio;
    ResultSet result;
    Connection conexion;


    /**
     *
     * @throws SQLException
     * sirve para ejecutar la interfaz y hara que se conecte a la BBDD
     */
    public Consultar() throws SQLException {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setBounds ( 400,150,600,300 );
        comboBox();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/ejerciciocosmeticos", "root", "");
        BuscarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    buscar();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });
        this.setVisible(true);

    }

    /**
     * Le damos al combobox los valores del enum
     */
    public void comboBox(){
        for (Buscar buscar : Buscar.values()){
            CBComboBuscar.addItem(buscar.getValor());
        }
        CBComboBuscar.setSelectedIndex(-1);
    }

    /**
     * Sirve para buscar es decir este metodo lleva la consultas para que cuando le demos al boton busque los productos en la BBDD
     * @throws SQLException
     */
    public void buscar() throws SQLException {
        if(String.valueOf(CBComboBuscar.getSelectedItem()).equals("productos")){
            EdadPrecio.setText("Precio");
            String sql = "SELECT * FROM productos WHERE Nombre=?";
            PreparedStatement sentencia=conexion.prepareStatement(sql);
            sentencia.setString(1,JTBuscarPorNombre.getText());
            result=sentencia.executeQuery();
            result.next();
            JTNombreBuscar.setText(result.getString("Precio"));
        }else{
            EdadPrecio.setText("Dni");
            String sql = "SELECT * FROM cliente WHERE Nombre=?";
            PreparedStatement sentencia=conexion.prepareStatement(sql);
            sentencia.setString(1,JTBuscarPorNombre.getText());
            result=sentencia.executeQuery();
            result.next();
            JTNombreBuscar.setText(result.getString("Dni"));
        }
    }


}
