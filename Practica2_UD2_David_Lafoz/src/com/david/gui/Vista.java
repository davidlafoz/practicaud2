package com.david.gui;

import com.david.base_enums.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Interfaz principal en el que se rellena visualiza borra modifica y las opciones
 */

public class Vista extends JFrame{
    public JPanel panel1;
    public JPanel PanelCliente;
    public JPanel PanelRecibo;
    public JPanel PanelProductos;
    public JPanel PanelTienda;
    public JTabbedPane tabbedPane1;
    public JPanel PanelFacial;
    public JPanel PanelCorporal;
    final static String TITULO="Ejercicio Cosmeticos";
    //


    public JComboBox CBIdTienda;
    public JTextField JTNombrePro;
    public JTextField JTPrecio;
    public JTextField JTModelo;
    public JComboBox CBMarca;
    public JTextField JTPh;
    public DatePicker FechaCaducidad;
    public JButton anadirProBtn;
    public JButton modificarProBtn;
    public JButton eliminarProBtn;
    public JTable tablaProductos;

    //CLIENTES
    public JTextField JTNombreCliente;
    public JTextField JTDni;
    public JTextField JTCP;
    public JTextField JTDireccion;
    public JComboBox CBCiudad;
    public JTextField JTTlfn;
    public JButton anadirClienteBtn;
    public JButton modificarClienteBtn;
    public JButton eliminarClienteBtn;
    public JTable tablaClientes;

    //RECIBO
    public JComboBox CBIdProducto;
    public JComboBox CBIdCliente;
    public JButton anadirReciboBtn;
    public JButton modificarReciboBtn;
    public JButton eliminarReciboBtn;
    public JTable tablaRecibo;
    public JTextField JTDireccionTienda;
    public JTextField JTPoblacionTienda;
    public JComboBox CBProvinciaTienda;

    //TIENDA
    public JTextField JTCPTienda;
    public JTextField JTTlfnTienda;
    public JButton anadirTiendaBtn;
    public JButton modificarTiendaBtn;
    public JButton eliminarTiendaBtn;
    public JTable tablaTienda;
    public JComboBox CBComunidadTienda;

    //FACIAL
    public JTextField JTPesoGramo;
    public JComboBox CBIDFacial;
    public JButton anadirFacialBtn;
    public JButton modificarFacialBtn;
    public JButton eliminarFacialBtn;
    public JTable tablaFacial;

    //CORPORAL
    public JComboBox CBIDcorporal;
    public JTextField JTCantidad;
    public JButton anadirCorporalBtn;
    public JButton modificarCorporalBtn;
    public JButton elimonarCorporalBtn;
    public JTable tablaCorporal;


    //DEFAULT TABLE MODEL
    DefaultTableModel dtmTienda;
    DefaultTableModel dtmProductos;
    DefaultTableModel dtmClientes;
    DefaultTableModel dtmRecibo;
    DefaultTableModel dtmFacial;
    DefaultTableModel dtmCorporal;


    //MENU ITEM

    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    JMenuItem itemBuscar;
    JMenuItem itemInfo;

    //ACCEDER PARA CONECTAR
    AccederParaConectar accederParaConectar;
    JDialog adminPasswordVentana;
    JButton entrar;
    JPasswordField adminPassword;


    /**
     * Para que se ejecute la interfaz y se vea
     */
    public Vista(){
        super(TITULO);
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setBounds ( 330,100,800,500 );
        this.setVisible(true);
        accederParaConectar = new AccederParaConectar(this);
        comboBox();
        tableModel();
        menu();
        solicituContrasena();

    }

    /**
     * Este metodo sirve para los datos del property se accede mediante opciones y tendremos que poner la contraseña
     * cuando entremos podremos cambiar los datos del archivo
     */

    private void solicituContrasena() {

        entrar=new JButton("Entrar");
        entrar.setActionCommand("AbrirConexion");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100,26));
        Object[] opciones = new Object[]{entrar,adminPassword};
        JOptionPane p1 = new JOptionPane("Introduce la contraseña",JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION,null,opciones);
        adminPasswordVentana = new JDialog(this,"Opciones",null);
        adminPasswordVentana.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        adminPasswordVentana.setContentPane(p1);
        adminPasswordVentana.pack();
        adminPasswordVentana.setLocationRelativeTo(this);



    }

    /**
     * Inicializamos todas las tablas de las pestañas donde se mostraran los datos
     */
    public void tableModel(){
        this.dtmTienda=new DefaultTableModel();
        this.tablaTienda.setModel(dtmTienda);

        this.dtmProductos=new DefaultTableModel();
        this.tablaProductos.setModel(dtmProductos);

        this.dtmClientes=new DefaultTableModel();
        this.tablaClientes.setModel(dtmClientes);

        this.dtmRecibo=new DefaultTableModel();
        this.tablaRecibo.setModel(dtmRecibo);

        this.dtmFacial=new DefaultTableModel();
        this.tablaFacial.setModel(dtmFacial);

        this.dtmCorporal=new DefaultTableModel();
        this.tablaCorporal.setModel(dtmCorporal);
    }

    /**
     * Daremos a cada combobox todos los valores de cada enum que les corrresponden
     */
    public void comboBox(){
        for (ComunidadTienda comunidad: ComunidadTienda.values()) {
            CBComunidadTienda.addItem(comunidad.getComunidad());
        }
        CBComunidadTienda.setSelectedIndex(-1);
        for (TiendaProvincia tienda : TiendaProvincia.values()) {
            CBProvinciaTienda.addItem(tienda.getProvincia());
        }
        CBProvinciaTienda.setSelectedIndex(-1);
        for (ClienteCiudad cliente: ClienteCiudad.values()){
            CBCiudad.addItem(cliente.getCiudad());
        }
        CBCiudad.setSelectedIndex(-1);
        for (MarcaProducto marca: MarcaProducto.values()) {
            CBMarca.addItem(marca.getMarca());
        }
        CBMarca.setSelectedIndex(-1);

    }

    /**
     * Este metodo sirve para crear el menu y sus opciones en las que apareceran en la interfaz principal
     */

    public void menu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");

        itemOpciones=new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");

        itemSalir=new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        itemBuscar=new JMenuItem("Buscar");
        itemBuscar.setActionCommand("Buscar");

        itemInfo = new JMenuItem("Info");
        itemInfo.setActionCommand("Info");



        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        menu.add(itemBuscar);
        menu.add(itemInfo);

        menuBar.add(menu);
        this.setJMenuBar(menuBar);
    }
}
