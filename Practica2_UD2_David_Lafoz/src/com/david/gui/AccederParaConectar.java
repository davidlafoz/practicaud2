package com.david.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Interfaz para conectar
 */
public class AccederParaConectar extends JDialog{
    public JPanel panel1;
    public JTextField tfIP;
    public JTextField tfUsuario;
    public JPasswordField jpPass;
    public JPasswordField jpPassAdmin;
    public JButton guardarButton;

    Frame owner;

    /**
     *
     * @param owner
     * Este metodo sirve para que se cree la interfaz
     */
    public AccederParaConectar(Frame owner) {
        super(owner,"opciones",true);
        this.owner=owner;
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize ( new Dimension ( this.getWidth ()+200,this.getHeight ()));
        this.setLocationRelativeTo(owner);

    }

}
