package com.david.gui;

import com.david.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.Vector;

/**
 * Clase controlador sirve para dar funcionalidad a todo y que funcione
 */
public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    Modelo modelo;
    Vista vista;
    Consultar consultar;
    boolean refrescar;


    /**
     * constructor para llamar a todos los metodos
     * @param modelo
     * @param vista
     * @throws SQLException
     */
    public Controlador(Modelo modelo, Vista vista) throws SQLException {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        addActionListener(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * creamos un metodo para llamar a todos los refrescar
     * @throws SQLException
     */
    public void refrescarTodo() throws SQLException {
        refrescarCliente();
        refrescarClienteProducto();
        refrescarCorporal();
        refrescarFacial();
        refrescarProductos();
        refrescarTienda();
        refrescar = false;

    }

    /**
     * Metodo para que funciones los botones y los menus
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            /**
             * Hace que sea visible la interfaz de perdir la contraseña
             */
            case "Opciones":
                vista.adminPasswordVentana.setVisible(true);
                break;
            /**
             * Cierra la conexion con la bbdd
             */
            case "Desconectar":
                try {
                    if(e.getActionCommand()=="Desconectar") {
                        modelo.desconectar();
                        vista.itemDesconectar.setText("Conectar");
                        vista.itemDesconectar.setActionCommand("Conectar");
                        System.out.println("Has desconectado");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Conecta con la bbdd
              */
            case "Conectar":
                if(e.getActionCommand()=="Conectar"){
                    modelo.conectar();
                    vista.itemDesconectar.setText("Desconectar");
                    vista.itemDesconectar.setActionCommand("Desconectar");
                    System.out.println("Has conectado");
                }
                break;
            /**
             * Sale de la aplicacion
              */
            case "Salir":
                int resp=Util.mensajeConfirmacion("Desea Salir","Salir");
                if(resp== JOptionPane.YES_OPTION){
                    System.exit(0);
                }
                break;
            /**
             * Sirve para abir la interfaz de busqueda y buscas un valor de la tabla de bbdd
             */
            case "Buscar":
                try {
                    consultar = new Consultar();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Abre la interfaz de contraseña y si es correcta abre la interfaz de ip contraseña usuario
             */
            case "AbrirConexion":
                if (String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getContrasenaAdmin())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordVentana.dispose();
                    vista.accederParaConectar.setVisible(true);

                }else{
                    Util.showErrorAlert("La contraseña no es valida");
                }
                break;
            /**
             * Lleva a la pagina de informacion de productos cosmeticos
             */
            case "Info":
                try {
                    Desktop.getDesktop().browse(new URL("https://www.cosmeticos24h.com").toURI());
                } catch (Exception e2) {}
                break;
            /**
             * Sirve para guardar los datos de la interfaz de la ip usuario contraseña en el fichero config.properties
             */
            case "Guardar":
                try {
                    modelo.setValoresLogin(vista.accederParaConectar.tfIP.getText(),
                            vista.accederParaConectar.tfUsuario.getText(),
                            String.valueOf(vista.accederParaConectar.jpPass.getPassword()),
                            String.valueOf(vista.accederParaConectar.jpPassAdmin.getPassword()));
                    vista.accederParaConectar.dispose();
                    vista.dispose();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para añadir un cliente en la bbdd
             */
            case "AnadirCliente":
                if(comprobarClenteVacio()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else if(modelo.clienteDniExiste(vista.JTDni.getText())){
                    Util.showErrorAlert("El DNI ya existe");
                }else{
                    modelo.insertarCliente(
                            vista.JTNombreCliente.getText(),
                            vista.JTDni.getText(),
                            Integer.parseInt(vista.JTCP.getText()),
                            vista.JTDireccion.getText(),
                            String.valueOf(vista.CBCiudad.getSelectedItem()),
                            Integer.parseInt(vista.JTTlfn.getText()));
                }
                try {
                    refrescarTodo();
                    borrarCamposClente();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar un cliente en la bbdd
             */
            case "ModificarCliente":
                try {
                    if (comprobarClenteVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarCliente(vista.JTNombreCliente.getText(),
                                vista.JTDni.getText(),
                                Integer.parseInt(vista.JTCP.getText()),
                                vista.JTDireccion.getText(),
                                String.valueOf(vista.CBCiudad.getSelectedItem()),
                                Integer.parseInt(vista.JTTlfn.getText()),
                                Integer.parseInt((String) vista.tablaClientes.getValueAt(vista.tablaClientes.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposClente();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar un cliente en la bbdd
             */
            case "EliminarCliente":
                try {
                    modelo.borrarCliente(Integer.parseInt((String) vista.tablaClientes.getValueAt(vista.tablaClientes.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposClente();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }

                break;
            /**
             * Sirve para añadir un corporal en la bbdd
             */
            case "AñadirCorporal":
                if(comprobarCorporalVacio()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else{
                    modelo.insertarCorporal(String.valueOf(vista.CBIDcorporal.getSelectedItem()),
                            Integer.parseInt(vista.JTCantidad.getText()));
                }
                try {
                   refrescarTodo();
                   borrarCamposCorporal();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar un corporal en la bbdd
             */
            case "ModificarCorporal":
                try {
                    if (comprobarCorporalVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                       modelo.modificarCorporal(String.valueOf(vista.CBIDcorporal.getSelectedItem()),
                               Integer.parseInt(vista.JTCantidad.getText()),
                                Integer.parseInt((String) vista.tablaCorporal.getValueAt(vista.tablaCorporal.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposCorporal();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar un corporal en la bbdd
             */
            case "EliminarCorporal":
                try {
                    modelo.borrarCorporal(Integer.parseInt((String) vista.tablaCorporal.getValueAt(vista.tablaCorporal.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposCorporal();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }
                break;

            /**
             * Sirve para añadir un facial en la bbdd
             */
            case "AñadirFacial":
                if(comprobarFacialVacio()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else{
                    modelo.insertarFacial(String.valueOf(vista.CBIDFacial.getSelectedItem()),
                            Float.parseFloat(vista.JTPesoGramo.getText()));
                }
                try {
                    refrescarTodo();
                    borrarCamposCorporal();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar un facial en la bbdd
             */
            case "ModificarFacial":
                try {
                    if (comprobarFacialVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarFacial(String.valueOf(vista.CBIDFacial.getSelectedItem()),
                                Float.parseFloat(vista.JTPesoGramo.getText()),
                                Integer.parseInt((String) vista.tablaFacial.getValueAt(vista.tablaFacial.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposFacial();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar un facial en la bbdd
             */
            case "EliminarFacial":
                try {
                    modelo.borrarFacial(Integer.parseInt((String) vista.tablaFacial.getValueAt(vista.tablaFacial.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposFacial();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }

                break;

            /**
             * Sirve para añadir un producto en la bbdd
             */
            case "AnadirProducto":
                if(comprobarProductosVacio()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else if(modelo.productosNombreExiste(vista.JTNombrePro.getText())) {
                    Util.showErrorAlert("El nombre del producto ya existe");
                    break;
                }else{
                    modelo.insertarProductos(String.valueOf(vista.CBIdTienda.getSelectedItem()),vista.JTNombrePro.getText(),
                            Float.parseFloat(vista.JTPrecio.getText()),vista.JTModelo.getText(),String.valueOf(vista.CBMarca.getSelectedItem()),
                            Float.parseFloat(vista.JTPh.getText()), Date.valueOf(vista.FechaCaducidad.getDate()));
                }
                try {
                    refrescarTodo();
                    borrarCamposProductos();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar un producto en la bbdd
             */
            case "ModificarProducto":
                try {
                    if (comprobarProductosVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarProductos(String.valueOf(vista.CBIdTienda.getSelectedItem()),vista.JTNombrePro.getText(),
                                Float.parseFloat(vista.JTPrecio.getText()),vista.JTModelo.getText(),String.valueOf(vista.CBMarca.getSelectedItem()),
                                Float.parseFloat(vista.JTPh.getText()), Date.valueOf(vista.FechaCaducidad.getDate()),
                                Integer.parseInt((String) vista.tablaProductos.getValueAt(vista.tablaProductos.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposProductos();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar un producto en la bbdd
             */
            case "EliminarProducto":
                try {
                    modelo.borrarProductos(Integer.parseInt((String) vista.tablaProductos.getValueAt(vista.tablaProductos.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposProductos();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }

                break;

            /**
             * Sirve para añadir un recibo en la bbdd
             */
            case "AnadirRecibo":
                if(comprobarClienteProductoVacio()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else{
                    modelo.insertarRecibo(String.valueOf(vista.CBIdCliente.getSelectedItem()),String.valueOf(vista.CBIdProducto.getSelectedItem()));
                     }
                try {
                    refrescarTodo();
                    borrarCamposClienteProducto();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar un recibo en la bbdd
             */
            case "ModificarRecibo":
                try {
                    if (comprobarClienteProductoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarRecibo(String.valueOf(vista.CBIdCliente.getSelectedItem()),String.valueOf(vista.CBIdProducto.getSelectedItem()),
                                Integer.parseInt((String) vista.tablaRecibo.getValueAt(vista.tablaRecibo.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposClienteProducto();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar un recibo en la bbdd
             */
            case "EliminarRecibo":
                try {
                    modelo.borrarRecibo(Integer.parseInt((String) vista.tablaRecibo.getValueAt(vista.tablaRecibo.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposClienteProducto();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }

                break;
            /**
             * Sirve para añadir una tienda en la bbdd
             */
            case "AnadirTienda":
                if(comprobarTiendaVacia()){
                    Util.showErrorAlert("Falta algun campo de rellenar");
                    break;
                }else{
                    modelo.insertarTienda(vista.JTDireccionTienda.getText(),
                            Integer.parseInt(vista.JTPoblacionTienda.getText()),
                            String.valueOf(vista.CBComunidadTienda.getSelectedItem()),
                            String.valueOf(vista.CBProvinciaTienda.getSelectedItem()),
                            Integer.parseInt(vista.JTCPTienda.getText()),
                            Integer.parseInt(vista.JTTlfnTienda.getText()));

                }
                try {
                    refrescarTodo();
                    borrarCamposTienda();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            /**
             * Sirve para modificar una tienda en la bbdd
             */
            case "ModificarTienda":
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                    } else {
                        modelo.modificarTienda(vista.JTDireccionTienda.getText(),
                                Integer.parseInt(vista.JTPoblacionTienda.getText()),
                                String.valueOf(vista.CBComunidadTienda.getSelectedItem()),
                                String.valueOf(vista.CBProvinciaTienda.getSelectedItem()),
                                Integer.parseInt(vista.JTCPTienda.getText()),
                                Integer.parseInt(vista.JTTlfnTienda.getText()),
                                Integer.parseInt((String) vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0)));
                        try {
                            refrescarTodo();
                            borrarCamposTienda();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    }
                }catch (NumberFormatException nfe){
                    Util.showErrorAlert("Introduce en los campos numeros los numeros");
                }
                break;
            /**
             * Sirve para eliminar una tienda en la bbdd
             */
            case "EliminarTienda":
                try {
                    modelo.borrarTienda(Integer.parseInt((String) vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0)));
                    try {
                        refrescarTodo();
                        borrarCamposTienda();
                    } catch (SQLException ex) {
                        Util.showErrorAlert("No se puede borrar ya que tiene union con otras tablas");
                    }
                }catch(ArrayIndexOutOfBoundsException a){
                    Util.showErrorAlert ( "Selecciona algo" );
                }

                break;
        }
    }


    /**
     * sirve para que cuando cliquemos en la tabla cambie los valores en cada caja de texto o comboBox
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting() && !((ListSelectionModel)e.getSource()).isSelectionEmpty()){
            if(e.getSource().equals(vista.tablaClientes.getSelectionModel())){
                int row = vista.tablaClientes.getSelectedRow();
                vista.JTNombreCliente.setText(String.valueOf(vista.tablaClientes.getValueAt(row,1)));
                vista.JTDni.setText(String.valueOf(vista.tablaClientes.getValueAt(row,2)));
                vista.JTCP.setText(String.valueOf(vista.tablaClientes.getValueAt(row,3)));
                vista.JTDireccion.setText(String.valueOf(vista.tablaClientes.getValueAt(row,4)));
                vista.CBCiudad.setSelectedItem(vista.tablaClientes.getValueAt(row,5));
                vista.JTTlfn.setText(String.valueOf(vista.tablaClientes.getValueAt(row,6)));
            }else if(e.getSource().equals(vista.tablaCorporal.getSelectionModel())){
                int row=vista.tablaCorporal.getSelectedRow();
                vista.CBIDcorporal.setSelectedItem(vista.tablaCorporal.getValueAt(row,1));
                vista.JTCantidad.setText(String.valueOf(vista.tablaCorporal.getValueAt(row,2)));
            }else if(e.getSource().equals(vista.tablaFacial.getSelectionModel())){
                int row=vista.tablaFacial.getSelectedRow();
                vista.CBIDFacial.setSelectedItem(vista.tablaFacial.getValueAt(row,1));
                vista.JTPesoGramo.setText(String.valueOf(vista.tablaFacial.getValueAt(row,2)));
            }else if(e.getSource().equals(vista.tablaProductos.getSelectionModel())){
                int row=vista.tablaProductos.getSelectedRow();
                vista.CBIdTienda.setSelectedItem(vista.tablaProductos.getValueAt(row,7));
                vista.JTNombrePro.setText(String.valueOf(vista.tablaProductos.getValueAt(row,1)));
                vista.JTPrecio.setText(String.valueOf(vista.tablaProductos.getValueAt(row,2)));
                vista.JTModelo.setText(String.valueOf(vista.tablaProductos.getValueAt(row,3)));
                vista.CBMarca.setSelectedItem(vista.tablaProductos.getValueAt(row,4));
                vista.JTPh.setText(String.valueOf(vista.tablaProductos.getValueAt(row,5)));
                vista.FechaCaducidad.setDate(LocalDate.parse(String.valueOf(vista.tablaProductos.getValueAt(row,6))));

            }else if(e.getSource().equals(vista.tablaTienda.getSelectionModel())){
                int row=vista.tablaTienda.getSelectedRow();
                vista.JTDireccionTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row,1)));
                vista.JTPoblacionTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row,2)));
                vista.CBComunidadTienda.setSelectedItem(vista.tablaTienda.getValueAt(row,3));
                vista.CBProvinciaTienda.setSelectedItem(vista.tablaTienda.getValueAt(row,4));
                vista.JTCPTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row,5)));
                vista.JTTlfnTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row,6)));

            }else if(e.getSource().equals(vista.tablaRecibo.getSelectionModel())){
                int row=vista.tablaRecibo.getSelectedRow();
                vista.CBIdCliente.setSelectedItem(vista.tablaRecibo.getValueAt(row,1));
                vista.CBIdProducto.setSelectedItem(vista.tablaRecibo.getValueAt(row,2));
            }
        }
    }

    /**
     * Sirve para que a la hora de pulsar un boton escuche y pueda funcionar
     * @param listener
     */

    private void addActionListener(ActionListener listener) {

        vista.anadirClienteBtn.addActionListener(listener);
        vista.anadirClienteBtn.setActionCommand("AnadirCliente");
        vista.anadirCorporalBtn.addActionListener(listener);
        vista.anadirCorporalBtn.setActionCommand("AñadirCorporal");
        vista.anadirFacialBtn.addActionListener(listener);
        vista.anadirFacialBtn.setActionCommand("AñadirFacial");
        vista.anadirProBtn.addActionListener(listener);
        vista.anadirProBtn.setActionCommand("AnadirProducto");
        vista.anadirReciboBtn.addActionListener(listener);
        vista.anadirReciboBtn.setActionCommand("AnadirRecibo");
        vista.anadirTiendaBtn.addActionListener(listener);
        vista.anadirTiendaBtn.setActionCommand("AnadirTienda");

        vista.modificarClienteBtn.addActionListener(listener);
        vista.modificarClienteBtn.setActionCommand("ModificarCliente");
        vista.modificarCorporalBtn.addActionListener(listener);
        vista.modificarCorporalBtn.setActionCommand("ModificarCorporal");
        vista.modificarFacialBtn.addActionListener(listener);
        vista.modificarFacialBtn.setActionCommand("ModificarFacial");
        vista.modificarProBtn.addActionListener(listener);
        vista.modificarProBtn.setActionCommand("ModificarProducto");
        vista.modificarReciboBtn.addActionListener(listener);
        vista.modificarReciboBtn.setActionCommand("ModificarRecibo");
        vista.modificarTiendaBtn.addActionListener(listener);
        vista.modificarTiendaBtn.setActionCommand("ModificarTienda");

        vista.eliminarClienteBtn.addActionListener(listener);
        vista.eliminarClienteBtn.setActionCommand("EliminarCliente");
        vista.elimonarCorporalBtn.addActionListener(listener);
        vista.elimonarCorporalBtn.setActionCommand("EliminarCorporal");
        vista.eliminarFacialBtn.addActionListener(listener);
        vista.eliminarFacialBtn.setActionCommand("EliminarFacial");
        vista.eliminarProBtn.addActionListener(listener);
        vista.eliminarProBtn.setActionCommand("EliminarProducto");
        vista.eliminarReciboBtn.addActionListener(listener);
        vista.eliminarReciboBtn.setActionCommand("EliminarRecibo");
        vista.eliminarTiendaBtn.addActionListener(listener);
        vista.eliminarTiendaBtn.setActionCommand("EliminarTienda");

        vista.accederParaConectar.guardarButton.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.entrar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemBuscar.addActionListener(listener);
        vista.itemInfo.addActionListener(listener);

        vista.tablaClientes.getSelectionModel().addListSelectionListener(this);
        vista.tablaTienda.getSelectionModel().addListSelectionListener(this);
        vista.tablaRecibo.getSelectionModel().addListSelectionListener(this);
        vista.tablaProductos.getSelectionModel().addListSelectionListener(this);
        vista.tablaFacial.getSelectionModel().addListSelectionListener(this);
        vista.tablaCorporal.getSelectionModel().addListSelectionListener(this);




    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void addItemListeners(Controlador controlador) {

    }

    /**
     * Sirve para refrescar la tabla cliente y el comboBox
     * @throws SQLException
     */
    private void refrescarCliente() throws SQLException {
        vista.tablaClientes.setModel(construirTablaCliente(modelo.consultarCliente()));
        vista.CBIdCliente.removeAllItems();
        for(int i=0; i<vista.dtmClientes.getRowCount();i++){
            vista.CBIdCliente.addItem(vista.dtmClientes.getValueAt(i,0)+" - "+
                    vista.dtmClientes.getValueAt(i,1));
        }
    }

    /**
     * Sirve para construir las columnas de la tabla cliente
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaCliente(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmClientes.setDataVector(data,nombreColumnas);

        return vista.dtmClientes;


    }

    /**
     * Sirve para refrescar la tabla tienda y el comboBox
     * @throws SQLException
     */
    private void refrescarTienda() throws SQLException {
        vista.tablaTienda.setModel(construirTablaTienda(modelo.consultarTienda()));
        vista.CBIdTienda.removeAllItems();
        for(int i=0; i<vista.dtmTienda.getRowCount();i++){
            vista.CBIdTienda.addItem(vista.dtmTienda.getValueAt(i,0));
        }
    }

    /**
     * Sirve para construir las columnas de la tabla tienda
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaTienda(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmTienda.setDataVector(data,nombreColumnas);

        return vista.dtmTienda;


    }

    /**
     * Sirve para refrescar la tabla producto y el comboBox
     * @throws SQLException
     */
    private void refrescarProductos() throws SQLException {
        vista.tablaProductos.setModel(construirProductos(modelo.consultarProductos()));
        vista.CBIdProducto.removeAllItems();
        for(int i=0; i<vista.dtmProductos.getRowCount();i++){
            vista.CBIdProducto.addItem(vista.dtmProductos.getValueAt(i,0)+" - "+
                    vista.dtmProductos.getValueAt(i,1));
        }

        vista.CBIDcorporal.removeAllItems();
        for(int i=0; i<vista.dtmProductos.getRowCount();i++){
            vista.CBIDcorporal.addItem(vista.dtmProductos.getValueAt(i,0)+" - "+
                    vista.dtmProductos.getValueAt(i,1));
        }

        vista.CBIDFacial.removeAllItems();
        for(int i=0; i<vista.dtmProductos.getRowCount();i++){
            vista.CBIDFacial.addItem(vista.dtmProductos.getValueAt(i,0)+" - "+
                    vista.dtmProductos.getValueAt(i,1));
        }
    }

    /**
     * Sirve para construir las columnas de la tabla producto
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirProductos(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmProductos.setDataVector(data,nombreColumnas);

        return vista.dtmProductos;


    }

    /**
     * Sirve para refrescar la tabla clienteProducto y el comboBox
     * @throws SQLException
     */

    private void refrescarClienteProducto() throws SQLException {
        vista.tablaRecibo.setModel(construirTablaClienteProducto(modelo.consultarRecibo()));
    }

    /**
     * Sirve para construir las columnas de la tabla clienteProducto
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaClienteProducto(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmRecibo.setDataVector(data,nombreColumnas);

        return vista.dtmRecibo;


    }

    /**
     * sirve para refrescar la tabla Facial
     * @throws SQLException
     */
    private void refrescarFacial() throws SQLException {
        vista.tablaFacial.setModel(construirTablaFacial(modelo.consultarFacial()));

    }

    /**
     * Sirve para construir las columnas de la tabla Facial
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaFacial(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmFacial.setDataVector(data,nombreColumnas);

        return vista.dtmFacial;


    }

    /**
     * sirve para refrescar la tabla Corporal
     * @throws SQLException
     */
    private void refrescarCorporal() throws SQLException {
        vista.tablaCorporal.setModel(construirTablaCorporal(modelo.consultarCorporal()));


    }

    /**
     * Sirve para construir las columnas de la tabla Corporal
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablaCorporal(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> nombreColumnas = new Vector<>();
        int contadorColumnas = metaData.getColumnCount();
        for(int colum=1; colum<=contadorColumnas;colum++){
            nombreColumnas.add(metaData.getColumnName(colum));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs,contadorColumnas,data);

        vista.dtmCorporal.setDataVector(data,nombreColumnas);

        return vista.dtmCorporal;


    }

    /**
     * Sirve para contar las columnas de cada tabla y saber el numero de columnas que tiene
     * @param rs
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Comprueba si hay algun campo vacio de cliente
     * @return
     */
    private boolean comprobarClenteVacio(){
        return vista.JTNombreCliente.getText().isEmpty() ||
                vista.JTDni.getText().isEmpty() ||
                vista.JTCP.getText().isEmpty() ||
                vista.JTDireccion.getText().isEmpty() ||
                vista.CBCiudad.getSelectedIndex()== -1 ||
                vista.JTTlfn.getText().isEmpty();
    }

    /**
     * Comprueba si hay algun campo vacio de tienda
     * @return
     */
    private boolean comprobarTiendaVacia(){
        return vista.JTDireccionTienda.getText().isEmpty() ||
                vista.JTPoblacionTienda.getText().isEmpty() ||
                vista.CBComunidadTienda.getSelectedIndex() == -1 ||
                vista.CBProvinciaTienda.getSelectedIndex()== -1 ||
                vista.JTCPTienda.getText().isEmpty() ||
                vista.JTTlfnTienda.getText().isEmpty();
    }

    /**
     * Comprueba si hay algun campo vacio de productos
     * @return
     */
    private boolean comprobarProductosVacio(){
        return vista.CBIdTienda.getSelectedIndex()==-1 ||
                vista.JTNombrePro.getText().isEmpty() ||
                vista.JTPrecio.getText().isEmpty() ||
                vista.JTModelo.getText().isEmpty() ||
                vista.CBMarca.getSelectedIndex()==-1 ||
                vista.JTPh.getText().isEmpty() ||
                vista.FechaCaducidad.getText().isEmpty();
    }

    /**
     * Comprueba si hay algun campo vacio de ClienteProducto
     * @return
     */
    private boolean comprobarClienteProductoVacio(){
        return vista.CBIdCliente.getSelectedIndex()==-1 ||
                vista.CBIdProducto.getSelectedIndex()==-1;

    }

    /**
     * Comprueba si hay algun campo vacio de Facial
     * @return
     */
    private boolean comprobarFacialVacio(){
        return vista.CBIDFacial.getSelectedIndex()==-1 ||
                vista.JTPesoGramo.getText().isEmpty();


    }

    /**
     * Comprueba si hay algun campo vacio de Corporal
     * @return
     */
    private boolean comprobarCorporalVacio(){
        return vista.CBIDcorporal.getSelectedIndex()==-1 ||
                vista.JTCantidad.getText().isEmpty();


    }

    /**
     * Borra todos el contenido de los campos de cliente
     */
    private void borrarCamposClente(){
                vista.JTNombreCliente.setText("");
                vista.JTDni.setText("");
                vista.JTCP.setText("");
                vista.JTDireccion.setText("");
                vista.CBCiudad.setSelectedIndex(-1);
                vista.JTTlfn.setText("");
    }

    /**
     * Borra todos el contenido de los campos de Tienda
     */
    private void borrarCamposTienda(){
                vista.JTDireccionTienda.setText("");
                vista.JTPoblacionTienda.setText("");
                vista.CBComunidadTienda.setSelectedIndex(-1);
                vista.CBProvinciaTienda.setSelectedIndex(-1);
                vista.JTCPTienda.setText("");
                vista.JTTlfnTienda.setText("");
    }

    /**
     * Borra todos el contenido de los campos de Productos
     */
    private void borrarCamposProductos(){
                vista.CBIdTienda.setSelectedIndex(-1);
                vista.JTNombrePro.setText("");
                vista.JTPrecio.setText("");
                vista.JTModelo.setText("");
                vista.CBMarca.setSelectedIndex(-1);
                vista.JTPh.setText("");
                vista.FechaCaducidad.setText("");
    }

    /**
     * Borra todos el contenido de los campos de ClienteProducto
     */
    private void borrarCamposClienteProducto(){
                vista.CBIdCliente.setSelectedIndex(-1);
                vista.CBIdProducto.setSelectedIndex(-1);

    }

    /**
     * Borra todos el contenido de los campos de facial
     */
    private void borrarCamposFacial(){
                vista.CBIDFacial.setSelectedIndex(-1);
                vista.JTPesoGramo.setText("");


    }

    /**
     * Borra todos el contenido de los campos de corporal
     */
    private void borrarCamposCorporal(){
                vista.CBIDcorporal.setSelectedIndex(-1);
                vista.JTCantidad.setText("");


    }


    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Sirve para que te salga una alerta a la hora de cerrar la aplicacion
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp=Util.mensajeConfirmacion("Desea Salir","Salir");
        if(resp== JOptionPane.YES_OPTION){
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


}