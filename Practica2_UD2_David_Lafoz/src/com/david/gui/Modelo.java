package com.david.gui;

import java.io.*;
import java.sql.*;
import java.util.Properties;

/**
 * Clase en el que almacena los metodos para la basa de datos
 */
public class Modelo {
    String ip;
    String usuario;
    String contrasena;
    String contrasenaAdmin;
    Connection conexion;

    /**
     * Constructor que llamamos a la clase valoresLogin para obtener los valores de usuarrio contraseña ip
     */
    public Modelo(){
        getValoresLogin();
    }

    /**
     * Metodo que sirve para conectar a la base de datos y leera el fichero de consultasJava
     */
    public void conectar(){
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/ejerciciocosmeticos", "root", "");
        } catch (SQLException e) {
            try {
                conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/ejerciciocosmeticos", "root", "");
                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String q : query) {
                    statement=conexion.prepareStatement(q);
                    statement.executeQuery();
                }
            } catch (SQLException | IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    /**
     * Metodo para cerrar la conexion con BBDD
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * Sirve para insertar un cliente en la bbdd
     * @param nombre
     * @param dni
     * @param cp
     * @param direccion
     * @param ciudad
     * @param tlf
     */
    public void insertarCliente(String nombre,String dni,int cp,String direccion,String ciudad, int tlf) {
        String sentenciaSQL="INSERT INTO cliente (Nombre,Dni,CP,Direccion,Ciudad,NumeroTlfn) VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1,nombre);
            sentencia.setString(2,dni);
            sentencia.setInt(3,cp);
            sentencia.setString(4,direccion);
            sentencia.setString(5,ciudad);
            sentencia.setInt(6,tlf);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sirve para insertar una tienda en la bbdd
     * @param direccion
     * @param poblacion
     * @param provincia
     * @param comunidad
     * @param cp
     * @param tlf
     */
    public void insertarTienda(String direccion,int poblacion,String provincia,String comunidad,int cp, int tlf) {
        String sentenciaSQL="INSERT INTO tienda (Direccion,Poblacion,Provincia,Comunidad,CP,Tlfn) VALUES (?,?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setString(1,direccion);
            sentencia.setInt(2,poblacion);
            sentencia.setString(3,comunidad);
            sentencia.setString(4,provincia);
            sentencia.setInt(5,cp);
            sentencia.setInt(6,tlf);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sirve para insertar un producto en la bbdd
     * @param tienda
     * @param nombre
     * @param precio
     * @param modelo
     * @param marca
     * @param ph
     * @param fechaCaducidad
     */
    public void insertarProductos(String tienda, String nombre, float precio, String modelo, String marca, float ph, Date fechaCaducidad) {
        String sentenciaSQL="INSERT INTO productos (idTienda,Nombre,Precio,Modelo,Marca,PH,FechaCaducidad) VALUES (?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;

        int idTienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idTienda);
            sentencia.setString(2,nombre);
            sentencia.setFloat(3,precio);
            sentencia.setString(4,modelo);
            sentencia.setString(5,marca);
            sentencia.setFloat(6,ph);
            sentencia.setDate(7,fechaCaducidad);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para insertar un recibo en la bbdd
     * @param cliente
     * @param producto
     */

    public void insertarRecibo(String cliente, String producto) {
        String sentenciaSQL="INSERT INTO clienteproducto (idCliente, idProducto) VALUES (?,?)";

        PreparedStatement sentencia = null;

        int idCliente = Integer.valueOf(cliente.split(" ")[0]);
        int idProducto = Integer.valueOf(producto.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idCliente);
            sentencia.setInt(2,idProducto);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sirve para insertar un corporal en la bbdd
     * @param productoCorporal
     * @param cantidad
     */
    public void insertarCorporal(String productoCorporal, int cantidad) {
        String sentenciaSQL="INSERT INTO corporal (idProductoCorporal, Cantidad) VALUES (?,?)";

        PreparedStatement sentencia = null;

        int idProductoCorporal = Integer.valueOf(productoCorporal.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idProductoCorporal);
            sentencia.setInt(2,cantidad);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sirve para insertar un facial en la bbdd
     * @param productoFacial
     * @param pesoGramo
     */
    public void insertarFacial(String productoFacial, float pesoGramo) {
        String sentenciaSQL="INSERT INTO facial (idProductoFacial, PesoGramo) VALUES (?,?)";

        PreparedStatement sentencia = null;

        int idProductoFacial = Integer.valueOf(productoFacial.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idProductoFacial);
            sentencia.setFloat(2,pesoGramo);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para modificar un cliente en la bbdd
     * @param nombre
     * @param dni
     * @param cp
     * @param direccion
     * @param ciudad
     * @param tlf
     * @param idCliente
     */

    public void modificarCliente(String nombre,String dni,int cp,String direccion,String ciudad, int tlf,int idCliente){

        String sentenciaSql = "UPDATE cliente SET Nombre=?,Dni=?,CP=?,Direccion=?,Ciudad=?,NumeroTlfn=? WHERE idCliente=?";
        PreparedStatement sentencia=null;

        try {
            sentencia=conexion.prepareStatement ( sentenciaSql );
            sentencia.setString ( 1,nombre );
            sentencia.setString ( 2,dni );
            sentencia.setInt ( 3,cp );
            sentencia.setString ( 4,direccion );
            sentencia.setString ( 5,ciudad );
            sentencia.setInt ( 6,tlf );
            sentencia.setInt ( 7,idCliente );
            sentencia.executeUpdate ();

        } catch (SQLException e) {
            e.printStackTrace ();
        }

    }

    /**
     * sirve para modificar una tienda en la bbdd
     * @param direccion
     * @param poblacion
     * @param provincia
     * @param comunidad
     * @param cp
     * @param tlf
     * @param idTienda
     */
    public void modificarTienda(String direccion,int poblacion,String provincia,String comunidad,int cp, int tlf,int idTienda){

        String sentenciaSql = "UPDATE tienda SET Direccion=?,Poblacion=?,Provincia=?,Comunidad=?,CP=?,Tlfn=? WHERE idTienda=?";
        PreparedStatement sentencia=null;

        try {
            sentencia=conexion.prepareStatement ( sentenciaSql );
            sentencia.setString ( 1,direccion );
            sentencia.setInt ( 2,poblacion );
            sentencia.setString ( 3,comunidad );
            sentencia.setString ( 4,provincia );
            sentencia.setInt ( 5,cp );
            sentencia.setInt ( 6,tlf );
            sentencia.setInt ( 7,idTienda );
            sentencia.executeUpdate ();

        } catch (SQLException e) {
            e.printStackTrace ();
        }

    }

    /**
     * sirve para modificar un producto en la bbdd
     * @param tienda
     * @param nombre
     * @param precio
     * @param modelo
     * @param marca
     * @param ph
     * @param fechaCaducidad
     * @param idProducto
     */
    public void modificarProductos(String tienda,String nombre,float precio,String modelo, String marca,float ph,Date fechaCaducidad,int idProducto){

        String sentenciaSql = "UPDATE productos SET idTienda=?,Nombre=?,Precio=?,Modelo=?,Marca=?,PH=?,FechaCaducidad=? WHERE idProducto=?";
        PreparedStatement sentencia=null;

        int idTienda = Integer.valueOf ( tienda.split ( " " )[0] );

        try {
            sentencia=conexion.prepareStatement ( sentenciaSql );
            sentencia.setInt ( 1,idTienda );
            sentencia.setString ( 2,nombre );
            sentencia.setFloat ( 3,precio );
            sentencia.setString ( 4,modelo );
            sentencia.setString ( 5,marca );
            sentencia.setFloat ( 6,ph );
            sentencia.setDate ( 7,fechaCaducidad );
            sentencia.setInt ( 8,idProducto );
            sentencia.executeUpdate ();

        } catch (SQLException e) {
            e.printStackTrace ();
        }

    }

    /**
     * sirve para modificar un recibo en la bbdd
     * @param cliente
     * @param producto
     * @param idClienteProducto
     */
    public void modificarRecibo(String cliente,String producto,int idClienteProducto){

        String sentenciaSql = "UPDATE clienteProducto SET idCliente=?,idProducto=? WHERE idClienteProducto=?";
        PreparedStatement sentencia=null;

        int idCliente = Integer.valueOf ( cliente.split ( " " )[0] );
        int idProducto = Integer.valueOf ( producto.split ( " " )[0] );

        try {
            sentencia=conexion.prepareStatement ( sentenciaSql );
            sentencia.setInt ( 1,idCliente );
            sentencia.setInt ( 2,idProducto );
            sentencia.setFloat ( 3,idClienteProducto );

            sentencia.executeUpdate ();

        } catch (SQLException e) {
            e.printStackTrace ();
        }

    }

    /**
     * sirve para modificar un corporal en la bbdd
     * @param productoCorporal
     * @param cantidad
     * @param idCorporal
     */
    public void modificarCorporal(String productoCorporal, int cantidad,int idCorporal) {
        String sentenciaSQL="UPDATE corporal SET idProductoCorporal=?, Cantidad=? WHERE idCorporal=?";
        PreparedStatement sentencia = null;

        int idProductoCorporal = Integer.valueOf(productoCorporal.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idProductoCorporal);
            sentencia.setInt(2,cantidad);
            sentencia.setInt ( 3,idCorporal);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para modificar un facial en la bbdd
     * @param productoFacial
     * @param pesoGramo
     * @param idFacial
     */
    public void modificarFacial (String productoFacial, float pesoGramo,int idFacial) {
        String sentenciaSQL="UPDATE facial SET idProductoFacial=?, pesoGramo=? WHERE idFacial=?";
        PreparedStatement sentencia = null;

        int idProductoFacial = Integer.valueOf(productoFacial.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idProductoFacial);
            sentencia.setFloat (2,pesoGramo);
            sentencia.setInt ( 3,idFacial );

            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * sirve para borrar un cliente en la bbdd
     * @param idCliente
     */
    public void borrarCliente(int idCliente){
        String sentenciaSQL="DELETE FROM cliente WHERE idCliente=?";
        PreparedStatement sentencia=null;

        try {
            sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idCliente);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * sirve para borrar una tienda en la bbdd
     * @param idTienda
     */
    public void borrarTienda(int idTienda){
        String sentenciaSQL="DELETE FROM tienda WHERE idTienda=?";
        try {
            PreparedStatement sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idTienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para borrar un producto en la bbdd
     * @param idProducto
     */
    public void borrarProductos(int idProducto){
        String sentenciaSQL="DELETE FROM productos WHERE idProducto=?";
        try {
            PreparedStatement sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idProducto);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para borrar un recibo en la bbdd
     * @param idClienteProducto
     */
    public void borrarRecibo(int idClienteProducto){
        String sentenciaSQL="DELETE FROM clienteproducto WHERE idClienteProducto=?";
        try {
            PreparedStatement sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idClienteProducto);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para borrar un corporal en la bbdd
     * @param idCorporal
     */
    public void borrarCorporal(int idCorporal){
        String sentenciaSQL="DELETE FROM corporal WHERE idCorporal=?";
        try {
            PreparedStatement sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idCorporal);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * sirve para borrar un facial en la bbdd
     * @param idFacial
     */
    public void borrarFacial(int idFacial){
        String sentenciaSQL="DELETE FROM facial WHERE idFacial=?";
        try {
            PreparedStatement sentencia=conexion.prepareStatement(sentenciaSQL);
            sentencia.setInt(1,idFacial);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sirve para darle nombre a los campos de la tabla cliente y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */

    ResultSet consultarCliente() throws SQLException {
        String sentenciaSql="SELECT concat(idCliente) AS 'ID', concat(Nombre) AS 'Nombre', concat(Dni) AS 'DNI'," +
                "concat(CP) AS 'CP',concat(Direccion) AS 'Direccion', concat(Ciudad) AS 'Ciudad', concat(NumeroTlfn) AS 'Telefono' FROM cliente";
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para darle nombre a los campos de la tabla tienda y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */
    ResultSet consultarTienda() throws SQLException {
        String sentenciaSql="SELECT concat(idTienda) AS 'ID', concat(Direccion) AS 'Direccion', concat(Poblacion) AS 'Poblacion'," +
                "concat (Comunidad) AS 'Comunidad',concat(Provincia) AS 'Provincia',concat(CP) AS 'CP', concat(Tlfn) AS 'Telefono' FROM tienda";
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para darle nombre a los campos de la tabla producto y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */
    ResultSet consultarProductos() throws SQLException {
        String sentenciaSql="SELECT concat(p.idProducto) AS 'ID', concat(p.Nombre) AS 'Nombre', concat(p.Precio) AS 'Precio'," +
                "concat(p.Modelo) AS 'Modelo',concat(p.Marca) AS 'Marca', concat(p.PH) AS 'PH', concat(p.FechaCaducidad) AS 'Fecha de caducidad', " +
                "concat(t.idTienda) AS 'Tienda' FROM productos AS p INNER JOIN tienda AS t ON t.idTienda = p.idTienda" ;
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para darle nombre a los campos de la tabla recibo y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */
    ResultSet consultarRecibo() throws SQLException {
        String sentenciaSql="SELECT concat(cp.idClienteProducto) AS 'ID', concat(c.idCliente, ' - ',c.Dni) AS 'Cliente', concat(p.idProducto, ' - ',p.Nombre) AS 'Producto'" +
                "FROM clienteproducto AS cp INNER JOIN cliente AS c ON c.idCliente = cp.idCliente INNER JOIN productos AS p on cp.idProducto = p.idProducto" ;
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para darle nombre a los campos de la tabla corporal y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */
    ResultSet consultarCorporal() throws SQLException {
        String sentenciaSql="SELECT concat(c.idCorporal) AS 'ID', concat(p.idProducto, ' - ',p.Nombre) AS 'Producto'," +
                "concat(c.Cantidad) AS 'Cantidad' FROM corporal AS c INNER JOIN productos AS p on c.idProductoCorporal = p.idProducto" ;
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para darle nombre a los campos de la tabla facial y mostrar todos los valores de los campos
     * @return
     * @throws SQLException
     */

    ResultSet consultarFacial() throws SQLException {
        String sentenciaSql="SELECT concat(f.idFacial) AS 'ID', concat(p.idProducto, ' - ',p.Nombre) AS 'Producto'," +
                "concat(f.PesoGramo) AS 'PesoGramo' FROM facial AS f INNER JOIN productos AS p on f.idProductoFacial = p.idProducto" ;
        PreparedStatement sentencia=conexion.prepareStatement(sentenciaSql);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;

    }

    /**
     * Sirve para saber si el dni del cliente que vas a introducir ya esta introducido en la bbdd
     * @param dni
     * @return
     */

    public boolean clienteDniExiste(String dni) {
        String funcionClienteDni = "SELECT existeDniCliente(?)";
        PreparedStatement function;
        boolean existeDni = false;
        try {
            function = conexion.prepareStatement(funcionClienteDni);
            function.setString(1, dni);
            ResultSet rs = function.executeQuery();
            rs.next();

            existeDni = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existeDni;
    }

    /**
     * Sirve para saber si el nombre del producto que vas a introducir ya esta introducido en la bbdd
     * @param nombre
     * @return
     */
    public boolean productosNombreExiste(String nombre) {
        String funcionProductoNombre = "SELECT existeNombreProductos(?)";
        PreparedStatement function;
        boolean existeNombre = false;
        try {
            function = conexion.prepareStatement(funcionProductoNombre);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            existeNombre = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existeNombre;
    }

    /**
     * sirve para leer el fichero de consultas y asi poder realizar las consultas
     * @return
     * @throws IOException
     */

    private String leerFichero() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("consultasJava.sql"));
        String linea;
        StringBuilder stringBuilder = new StringBuilder();
        while((linea=reader.readLine())!=null){
            stringBuilder.append(linea);
            stringBuilder.append(" ");
        }
        return stringBuilder.toString();

    }

    /**
     * sirve para ñeer el fichero config y obtener los datos de cada variable
     */

    public void getValoresLogin() {

        InputStream leer = null;
        Properties datos = new Properties();
        String nombreFichero="config.properties";

        try {
            leer= new FileInputStream(nombreFichero);
            datos.load(leer);

            ip=datos.getProperty("ip");
            usuario=datos.getProperty("usuario");
            contrasena=datos.getProperty("contrasena");
            contrasenaAdmin=datos.getProperty("admin");

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Sirve para enviarle los valores obtenidos a las variables puestas
     * @param ip
     * @param usuario
     * @param contrasena
     * @param contrasenaAdmin
     * @throws IOException
     */

    public void setValoresLogin( String ip,String usuario,String contrasena,String contrasenaAdmin) throws IOException {
        Properties datos = new Properties();
        datos.setProperty("ip",ip);
        datos.setProperty("usuario",usuario);
        datos.setProperty("contrasena",contrasena);
        datos.setProperty("admin",contrasenaAdmin);

        OutputStream escribir = new FileOutputStream("config.properties");
        datos.store(escribir,null);


        this.ip=ip;
        this.usuario=usuario;
        this.contrasena=contrasena;
        this.contrasenaAdmin=contrasenaAdmin;
    }


    public String getIp() {
        return ip;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public String getContrasenaAdmin() {
        return contrasenaAdmin;
    }
}
