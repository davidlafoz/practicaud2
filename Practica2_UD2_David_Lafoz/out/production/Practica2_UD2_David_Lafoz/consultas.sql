CREATE DATABASE ejercicioCosmeticos;
USE ejercicioCosmeticos;

CREATE TABLE cliente(
    idCliente int AUTO_INCREMENT PRIMARY KEY,
    Nombre varchar (50) NOT NULL,
    Dni varchar (9) UNIQUE NOT NULL,
    CP int NOT NULL,
    Direccion varchar (100) NOT NULL,
    Ciudad varchar (40) NOT NULL,
    NumeroTlfn int NOT NULL

);

CREATE TABLE tienda(
    idTienda int AUTO_INCREMENT PRIMARY  KEY,
    Direccion varchar(40) NOT NULL ,
    Poblacion int NOT NULL,
    Comunidad varchar(40) NOT NULL,
    Provincia varchar (40) NOT NULL ,
    CP int NOT NULL ,
    Tlfn int NOT NULL
);


CREATE TABLE productos(
    idProducto int AUTO_INCREMENT PRIMARY KEY,
    idTienda int NOT NULL,
    Nombre varchar (40) NOT NULL UNIQUE,
    Precio float NOT NULL,
    Modelo varchar (50) NOT NULL,
    Marca varchar(50) NOT NULL,
    PH float NOT NULL,
    FechaCaducidad DATE NOT NULL,
    FOREIGN KEY (idTienda) REFERENCES tienda (idTienda)

);

CREATE TABLE clienteProducto(
    idClienteProducto int AUTO_INCREMENT PRIMARY KEY,
    idCliente int NOT NULL,
    idProducto int NOT NULL,
    FOREIGN KEY (idCliente) REFERENCES cliente (idCliente),
    FOREIGN KEY (idProducto) REFERENCES productos (idProducto)
);

CREATE TABLE corporal(
    idCorporal int AUTO_INCREMENT PRIMARY  KEY,
    idProductoCorporal int  NOT NULL,
    Cantidad int NOT NULL,
    FOREIGN KEY (idProductoCorporal) REFERENCES productos (idProducto)
);
CREATE TABLE facial(
    idFacial int AUTO_INCREMENT PRIMARY  KEY,
    idProductoFacial int NOT NULL,
    PesoGramo float NOT NULL,
    FOREIGN KEY (idProductoFacial) REFERENCES productos (idProducto)

);



delimiter ||
create function existeDniCliente(fDni varchar(9))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idCliente) from cliente)) do
    if  ((select Dni from cliente where idCliente = (i + 1)) like fDni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;
--
delimiter ||
create function existeNombreProductos(fNombre varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    while ( i < (select max(idProducto) from productos)) do
    if  ((select Nombre from productos where idProducto = (i + 1)) like fNombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end; ||
delimiter ;



